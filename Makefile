VERSION ?= v1.0.0-aplha.3

eq = $(if $(or $(1),$(2)),$(and $(findstring $(1),$(2)),\
                                $(findstring $(2),$(1))),1)

commit-message = $(shell grep 'VERSION ?= ' 'version' | tr -d 'VERSION ?= ')

docs:
	mkdir tmp_docs && cp -r content/* tmp_docs

release.pages:
	mkdir -p ${CI_COMMIT_REF_NAME}/docs && cp -r content/* ${CI_COMMIT_REF_NAME}/docs && \
		git clone -b _pages --single-branch git@gitlab.com:alnikyur/new-docs.wiki.git docs && \
			cp -r ${CI_COMMIT_REF_NAME} docs && cd docs && \
				ls -l && \
				git add -A && \
					git commit -am "Commit message ${CI_COMMIT_REF_NAME}" && \
						git push origin _pages

test.content:
	mkdir _docs && cp -r content/* _docs

#CI_COMMIT_REF_NAME = v1.0.0-alpha.3

test:
ifeq ($(tag),edge)
	git clone -b _pages --single-branch git@gitlab.com:alnikyur/new-docs.wiki.git _tmp_docs && \
		rm -rf _tmp_docs/edge/docs/* && \
		cp -r _docs/* _tmp_docs/edge/docs && \
		cd _tmp_docs && \
			git pull && \
			git add -A && \
			[ ! $$(git status -sb | wc -l) == '1' ] && git commit -m 'Update documentation for project ${CI_PROJECT_PATH} version edge' && \
			git push origin _pages || echo "No changes"; exit 0
else ifeq ($(tag),latest)
	git clone -b _pages --single-branch git@gitlab.com:alnikyur/new-docs.wiki.git _tmp_docs && \
		mkdir -p ${CI_COMMIT_REF_NAME}/docs && \
		cp -r _docs/* ${CI_COMMIT_REF_NAME}/docs && \
		mv ${CI_COMMIT_REF_NAME} _tmp_docs && \
		rm -rf _tmp_docs/latest/* && \
		cp -r _tmp_docs/${CI_COMMIT_REF_NAME}/* _tmp_docs/latest/ && \
		cd _tmp_docs && \
			git pull && \
			git add -A && \
			git commit -m 'Release documentation for project ${CI_PROJECT_PATH} version ${CI_COMMIT_REF_NAME}' && \
			git push origin _pages
endif

